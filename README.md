# pdtemplatenuxt2
## Nuxt SSR deployed to Google Firebase.
### author: russell clark
russell@russell-clark.it

### Source article
https://ben-mayer.com/blog/building-a-web-app-using-nuxtjs-and-firebase  
https://github.com/benmayer/nuxt-ssr-firebase-template

### A cry for help
I have spent days researching how to deploy Nuxt to Firebase  
As a static app its easy.  
As SSR app it beats me.  
You can see the outcome here: https://pdtemplatenuxt2test.web.app/  
The public repo source code is here: https://pdtemplatenuxt2test.web.app/  
I have included the functions & public, src & src/.nuxt directories for reference purposes  

Any assistance you help me & utold number who are trying to get this to work.

For Nuxt see:  [Nuxt.js docs](https://nuxtjs.org).

## Start Here
1) get yourself a Firebase account, create a project (on the Blaze plan - its not the free tier so set up some budget limits if you are unhappy)  

2) (Developed on a Ububtu 18.04 VM)
   
3) Follow the installation.  

## Installation
$ npm install -g firebase-tools  

$ mkdir Nuxt  
$ cd Nuxt  
Nuxt$ mkdir pdtemplatenuxt2  
Nuxt$ cd pdtemplatenuxt2  
Nuxt/pdtemplatenuxt2$ firebase login  
Nuxt/pdtemplatenuxt2$ firebase init  

... follow the instructions.  
Choose Firebase functions and hosting, funcs & deploy emulators - very useful.  
Choose the project you set up in the first step, or create new one, and give it an ID and name. This will be your project in the Google Firebase Console.  
Choose Javascript
Skip ESLint  
Don't install dependencies now, as we'll do that later
Choose 'public' as your default public folder. (This is where the static files go later)  
Don't configure as single page app (don't rewrite all urls to the index.html files)  
Skip automatic builds and deploys with Github  

### Init the app
Nuxt/pdtemplatenuxt2$ npm init nuxt-app src // creates project in src folder

#### Update the following files
``` js 
// functions.index.js
const functions = require('firebase-functions');
const { Nuxt } = require('nuxt-start');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

// const nuxtConfig = require('../src/nuxt.config.js');

const config = {
// ...nuxtConfig,
dev: false,
debug: false,
};

const nuxt = new Nuxt(config);

exports.ssrapp = functions.https.onRequest(async (req, res) => {
await nuxt.ready();
nuxt.render(req, res);
});
```
``` json
// firebase.json
{
"functions": {
"source": "functions"
},
"hosting": {
"public": "public",
"ignore": [
"firebase.json",
"**/.*",
"**/node_modules/**"
]
},
"rewrites": [
{
"source": "**",
"function": "ssrapp"
}
],
"emulators": {
"functions": {
"port": 5001
},
"hosting": {
"port": 5000
},
"ui": {
"enabled": true
}
}
}
```

``` json
// functions/package.json
{
"name": "functions",
"description": "Cloud Functions for Firebase",
"scripts": {
"emulators": "firebase emulators:start",
"shell": "firebase functions:shell",
"start": "npm run shell",
"deploy": "firebase deploy --only functions",
"logs": "firebase functions:log"
},
"engines": {
"node": "12"
},
"main": "index.js",
"dependencies": {
"firebase-admin": "^9.2.0",
"firebase-functions": "^3.11.0",
"nuxt-start": "^2.15.2"
},
"devDependencies": {
"firebase-functions-test": "^0.2.0"
},
"private": true
}
```

``` json
// src/package.json

{
"name": "pdtemplatenuxt2",
"description": "Template for Nuxt.js SSR app, deployed to Firebase functions.",
"author": "rrc",
"private": true,
"license": "MIT",
"scripts": {
      "dev": "nuxt",
      "build": "nuxt build",
      "start": "nuxt start",
      "generate": "nuxt generate"
  },
"deploy": "firebase deploy"
},
"engines": {},
"dependencies": {},
"devDependencies": {

}
}
```
#### Issue these commands
```cmd
// fire off these commands IN THE APPROPRIATE DIRECTORY:
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2/src$ yarn install
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2/functions$ yarn install
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2/functions$ yarn add nuxt-start

russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2$/src$ yarn build
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2$ mkdir -p functions/.nuxt/dist/

russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2$ cp -r src/.nuxt/dist/server functions/.nuxt/dist/
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2$ rm public
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2$ mkdir -p public/_nuxt
russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2$ cp -r src/.nuxt/dist/client/ public/_nuxt && cp -r src/static/ public/

// Fire up the emulators & follow the links. Very nice.    
// I can get functions to work. HOSTING does not:  

russell@RRCUbuntu18:~/Nuxt/pdtemplatenuxt2/functions$ yarn emulators
```

#### Deploy to Firebase
The final step: 

``` cmd
$ firebase deploy
```
